## aosp_RMX1801-userdebug 11 RD2A.211001.002 1633708128 test-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: RMX1801
- Brand: oppo
- Flavor: aosp_RMX1801-userdebug
- Release Version: 11
- Id: RD2A.211001.002
- Incremental: 1633708128
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oppo/RMX1801/RMX1801:11/RD2A.211001.002/1548:userdebug/release-keys
- OTA version: 
- Branch: aosp_RMX1801-userdebug-11-RD2A.211001.002-1633708128-test-keys
- Repo: oppo_rmx1801_dump_31703


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
